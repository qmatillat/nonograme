#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Cell {
    Unknow,
    Denied,
    Set,
}

impl Cell {
    pub fn as_char(&self) -> char {
        match self {
            Cell::Unknow => ' ',
            Cell::Denied => ' ',
            Cell::Set => '#',
        }
    }
}

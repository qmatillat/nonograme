pub type Constraint = Vec<usize>;
pub type ConstraintList = Vec<Constraint>;

#[derive(Debug, PartialEq, Eq)]
pub struct NonogrameHeader {
    pub vert: ConstraintList,
    pub horz: ConstraintList,
}

impl NonogrameHeader {
    pub fn size(&self) -> (usize, usize) {
        (self.horz.len(), self.vert.len())
    }

    pub fn constraints(&self) -> impl Iterator<Item = &ConstraintList> {
        vec![&self.horz, &self.vert].into_iter()
    }
}

use ndarray::{Array2, ArrayView1, Axis, Dimension, Ix2};
use std::fmt;
use std::iter::IntoIterator;

use crate::game_logic::{check_row_doable, tighten_row};

use super::{game_logic::check_row_valid, Cell, NonogrameHeader};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Nonograme<'a> {
    header: &'a NonogrameHeader,

    grid: Array2<Cell>,
}

impl<'a> std::hash::Hash for Nonograme<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.grid.hash(state)
    }
}

type Pos = <Ix2 as Dimension>::Pattern;

impl<'a> Nonograme<'a> {
    pub fn new(header: &'a NonogrameHeader, denied_pos: impl Iterator<Item = Pos>) -> Self {
        let mut grid = Array2::from_elem(header.size(), Cell::Unknow);

        for p in IntoIterator::into_iter(denied_pos) {
            grid[p] = Cell::Denied;
        }

        Self { header, grid }
    }

    fn with_constraint(&self) -> impl Iterator<Item = (ArrayView1<Cell>, &Vec<usize>)> + '_ {
        self.grid
            .axes()
            .zip(self.header.constraints())
            .flat_map(move |(axis, constraint_list)| {
                self.grid.axis_iter(axis.axis).zip(constraint_list)
            })
    }

    pub fn is_solved(&self) -> bool {
        self.with_constraint()
            .all(|(line, constraint)| check_row_valid(constraint, &line))
    }

    pub fn is_solvable(&self) -> bool {
        self.with_constraint()
            .all(|(line, constraint)| check_row_doable(constraint, &line))
    }

    pub fn tighten(&mut self) -> bool {
        let axis: Vec<_> = self.grid.axes().zip(self.header.constraints()).collect();

        let mut changed = false;
        for (axis, constraint_list) in axis {
            for (mut line, constraint) in self.grid.axis_iter_mut(axis.axis).zip(constraint_list) {
                changed |= tighten_row(&mut line, constraint)
            }
        }

        changed
    }

    pub fn solve(&mut self) {
        while self.tighten() {}
    }
}

impl<'a> fmt::Display for Nonograme<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let max_vert = self.header.vert.iter().map(Vec::len).max().unwrap();
        let max_horz = self.header.horz.iter().map(Vec::len).max().unwrap();

        for curr_line in (0..max_vert).rev() {
            for _ in 0..max_horz {
                write!(f, "   ")?;
            }
            for (id, constraint) in self.header.vert.iter().enumerate() {
                if id % 5 == 0 {
                    write!(f, " ")?;
                }

                if constraint.len() > curr_line {
                    write!(f, "{:^3}", constraint[constraint.len() - curr_line - 1])?;
                } else {
                    write!(f, "   ")?;
                }
            }
            writeln!(f)?;
        }

        for (idx, (constraint, line)) in self
            .header
            .horz
            .iter()
            .zip(self.grid.axis_iter(Axis(0)))
            .enumerate()
        {
            if idx % 5 == 0 {
                for _ in 0..max_horz {
                    write!(f, "   ")?;
                }

                for idy in 0..self.header.vert.len() {
                    if idy % 5 == 0 {
                        write!(f, "+")?;
                    }
                    write!(f, "---")?;
                }
                writeln!(f, "+")?;
            }

            for col in (0..max_horz).rev() {
                if constraint.len() > col {
                    write!(f, "{:^3}", constraint[constraint.len() - col - 1])?;
                } else {
                    write!(f, "   ")?;
                }
            }

            for (idy, cell) in line.iter().enumerate() {
                if idy % 5 == 0 {
                    write!(f, "|")?;
                }
                write!(f, " {} ", cell.as_char())?;
            }

            writeln!(f, "|")?;
        }

        for _ in 0..max_horz {
            write!(f, "   ")?;
        }

        for idy in 0..self.header.vert.len() {
            if idy % 5 == 0 {
                write!(f, "+")?;
            }
            write!(f, "---")?;
        }
        writeln!(f, "+")?;

        Ok(())
    }
}

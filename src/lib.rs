use std::convert::TryInto;

mod cell;
mod game_logic;
mod nonograme;
mod nonograme_header;

use cell::Cell;

pub use crate::nonograme::Nonograme;
pub use nonograme_header::NonogrameHeader;
pub use nonograme_header::{Constraint, ConstraintList};

pub fn parse_constraint(src: &str) -> ConstraintList {
    let mut ret = Vec::new();
    let mut curr = Vec::new();
    let mut nb = 0;

    for c in src.chars() {
        match c {
            ' ' => {
                curr.push(nb);
                nb = 0;
            }
            v @ '0'..='9' => {
                // Both unwrap are safe, because v is a single digit
                let read: usize = v.to_digit(10).unwrap().try_into().unwrap();
                nb = nb * 10 + read;
            }
            ',' => {
                curr.push(nb);
                nb = 0;
                let c = std::mem::take(&mut curr);
                ret.push(c);
            }

            _ => (), // Ingore other chars
        }
    }

    curr.push(nb);
    let c = std::mem::take(&mut curr);
    ret.push(c);

    ret
}

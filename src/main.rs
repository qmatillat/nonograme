pub use nonograme::{parse_constraint, Nonograme, NonogrameHeader};

fn main() -> Result<(), &'static str> {
    let header = NonogrameHeader {
        vert: parse_constraint("6,2 2 2,1 1 1,2 2 2 2,5 3 1,4 1 3 1,3 2 4 1,2 3 8,2 3 3,3 2 2,2 1 1 2,3 7,2 2 3 4,3 9 3,2 1 3 3 2,1 2 3 5,1 6 2 3,9 4 3,9 6,2 13"),
        horz: parse_constraint("6,2 9,6 1 1 1,4 1 5,2 5,1 1 4,2 4 2 3,3 3 1 4,2 2 2 4,1 1 2 3,4 2 3 3,2 2 2 1 2,1 1 2 3 1,2 1 3 2 1,3 2 2 2 3,5 2 2 3,5 2 2 2,7 3 5,1 3 9,3 9"),
    };
    let mut game = Nonograme::new(&header, vec![].into_iter());

    println!("=== Input:\n{}", game);
    if !game.is_solvable() {
        return Err("This game seem not solvable. Check your inputs");
    }
    game.solve();
    println!("=== Solved:\n{}", game);

    if !game.is_solved() {
        return Err("Unable to solve this game");
    }

    Ok(())
}

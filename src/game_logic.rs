use ndarray::{ArrayView1, ArrayViewMut1};

use crate::cell::Cell;

/// Check if a row is finished
pub fn check_row_valid(constraint: &[usize], line: &ArrayView1<Cell>) -> bool {
    let mut curr_suite: Option<usize> = None;

    let mut constraint = constraint.iter();

    for cell in line.iter() {
        match cell {
            Cell::Unknow | Cell::Denied => {
                if let Some(v) = curr_suite.take() {
                    if Some(&v) != constraint.next() {
                        return false;
                    }
                }
            }
            Cell::Set => *curr_suite.get_or_insert(0) += 1,
        }
    }

    if let Some(v) = curr_suite.take() {
        if Some(&v) != constraint.next() {
            return false;
        }
    }
    constraint.next().is_none()
}

/// Check if a row seem doable
pub fn check_row_doable(constraint: &[usize], line: &ArrayView1<Cell>) -> bool {
    check_row_possible(constraint, line) && check_row_not_over(constraint, line)
}

/// Check that all chunks are smaller than the maximum allowed size,
/// and that the sum of all Set is smaller than all constraints
fn check_row_not_over(constraint: &[usize], line: &ArrayView1<Cell>) -> bool {
    let mut max = 0;
    let mut sum = 0;
    let mut curr_suite: Option<usize> = None;

    for cell in line.iter() {
        match cell {
            Cell::Unknow | Cell::Denied => {
                if let Some(v) = curr_suite.take() {
                    max = max.max(v);
                }
            }
            Cell::Set => {
                *curr_suite.get_or_insert(0) += 1;
                sum += 1;
            }
        }
    }

    if let Some(v) = curr_suite.take() {
        max = max.max(v);
    }

    *constraint.iter().max().unwrap() >= max && constraint.iter().sum::<usize>() >= sum
}

/// Check that there is enough space in this row to be able to match the constrainst
fn check_row_possible(constraint: &[usize], line: &ArrayView1<Cell>) -> bool {
    let mut curr_suite: Option<usize> = None;

    let mut constraint = constraint.iter().peekable();

    for cell in line.iter() {
        match cell {
            Cell::Denied => {
                if let Some(mut v) = curr_suite.take() {
                    while let Some(&&c) = constraint.peek() {
                        if c <= v {
                            v = v.saturating_sub(c).saturating_sub(1);
                            constraint.next();
                        } else {
                            break;
                        }
                    }
                }
            }
            Cell::Unknow | Cell::Set => *curr_suite.get_or_insert(0) += 1,
        }
    }

    if let Some(mut v) = curr_suite.take() {
        while let Some(&&c) = constraint.peek() {
            if c <= v {
                v = v.saturating_sub(c).saturating_sub(1);
                constraint.next();
            } else {
                break;
            }
        }
    }

    constraint.next().is_none()
}

pub fn tighten_row(line: &mut ArrayViewMut1<Cell>, constraint: &[usize]) -> bool {
    fn gather_pos(line: &[Cell], constraint: &[usize]) -> Option<Vec<Cell>> {
        match constraint.split_first() {
            Some((curr, constraint)) => {
                if line.len() < *curr {
                    return None;
                }

                let mut ret = None;

                'lop: for chunk_start in 0..=(line.len() - curr) {
                    let (pre, mut left) = line.split_at(chunk_start + curr);
                    let mut pre = pre.to_owned();

                    // Empty part
                    for cell in &mut pre[..chunk_start] {
                        match cell {
                            Cell::Set => {
                                return ret;
                            }

                            Cell::Denied => (),
                            Cell::Unknow => *cell = Cell::Denied,
                        }
                    }

                    // Set part
                    for cell in &mut pre[chunk_start..] {
                        match cell {
                            Cell::Denied => continue 'lop,

                            Cell::Unknow => *cell = Cell::Set,
                            Cell::Set => (),
                        }
                    }

                    // Secure a empty slot after chunk
                    // If non, we reached the end
                    if let Some((cell, sline)) = left.split_first() {
                        match cell {
                            Cell::Set => continue 'lop,
                            Cell::Unknow | Cell::Denied => (),
                        }

                        left = sline;
                        pre.push(Cell::Denied);
                    }

                    let out = gather_pos(left, constraint);
                    let out = if let Some(out) = out {
                        out
                    } else {
                        continue 'lop;
                    };

                    pre.extend_from_slice(&out);

                    match &mut ret {
                        Some(ret) => {
                            assert_eq!(ret.len(), pre.len());
                            assert_eq!(ret.len(), line.len());
                            for ((mut ret, pre), line) in ret.iter_mut().zip(pre).zip(line) {
                                if matches!(line, Cell::Denied | Cell::Set) {
                                    assert_eq!(ret, line);
                                    assert_eq!(&pre, line);
                                };

                                match (&mut ret, &pre) {
                                    // Same value on both side, we keep them
                                    (Cell::Denied, Cell::Denied) | (Cell::Set, Cell::Set) => (),
                                    // Originally Set or Denied, but the other is not the same value
                                    (Cell::Set, Cell::Unknow | Cell::Denied)
                                    | (Cell::Denied, Cell::Unknow | Cell::Set) => {
                                        *ret = Cell::Unknow
                                    }
                                    // Already unknown
                                    (Cell::Unknow, _) => (),
                                }
                            }
                        }
                        None => ret = Some(pre),
                    }
                }

                ret
            }

            None => {
                // Check if they all can be denied
                if line.iter().any(|c| c == &Cell::Set) {
                    None
                } else {
                    Some(vec![Cell::Denied; line.len()])
                }
            }
        }
    }

    if let Some(out) = gather_pos(&line.to_vec(), constraint) {
        assert_eq!(out.len(), line.len());

        let mut changed = false;
        for (line, cell) in line.iter_mut().zip(out.into_iter()) {
            changed |= line != &cell;
            *line = cell;
        }

        changed
    } else {
        false
    }
}

#[cfg(test)]
mod tests {
    use ndarray::{array, Array1};
    use Cell::*;

    use super::*;

    #[test]
    fn valid() {
        assert!(check_row_valid(
            &vec![1, 2, 1],
            &Array1::from_vec(vec![Set, Unknow, Set, Set, Unknow, Set]).view()
        ));

        assert!(!check_row_valid(
            &vec![1, 3, 1],
            &Array1::from_vec(vec![Set, Unknow, Set, Set, Unknow, Set]).view()
        ));

        assert!(!check_row_valid(
            &vec![1, 2, 1, 1],
            &Array1::from_vec(vec![Set, Unknow, Set, Set, Unknow, Set]).view()
        ));
    }

    #[test]
    fn doable() {
        assert!(check_row_doable(
            &vec![1, 2, 1],
            &Array1::from_vec(vec![Set, Unknow, Set, Set, Unknow, Set]).view()
        ));

        assert!(!check_row_doable(
            &vec![1],
            &Array1::from_vec(vec![Set, Unknow, Set]).view()
        ));
    }

    #[test]
    fn tighten() {
        let mut row = Array1::from_vec(vec![Unknow; 6]);
        tighten_row(&mut row.view_mut(), &vec![1, 2, 1]);

        assert_eq!(row, array![Set, Denied, Set, Set, Denied, Set]);
    }

    #[test]
    fn tighten2() {
        let mut row = Array1::from_vec(vec![Unknow; 7]);
        tighten_row(&mut row.view_mut(), &vec![2, 3]);

        assert_eq!(row, array![Unknow, Set, Unknow, Unknow, Set, Set, Unknow]);
    }

    #[test]
    fn tighten3() {
        let mut row = Array1::from_vec(vec![Unknow; 3]);
        tighten_row(&mut row.view_mut(), &vec![1]);

        assert_eq!(row, array![Unknow, Unknow, Unknow]);
    }
}
